%define distro rosa

%define modules memfreetest pcietest nlkm

%{?build_selinux}%{?!build_selinux:%bcond_with selinux}

Summary: Binary utilities for ROSA Tests
Name: rosa-test-tools
Version: 0.1
Release: 1
License: GPL
Group: System/Base
URL: https://abf.io/kernelplv/rosa-test-suite-dev

%define dkms_version %{version}
%define app %{name}-%{version}

Source0: %{url}/archive/%{name}-dev-%{version}.tar.gz

BuildRequires: autoconf
BuildRequires: gcc
BuildRequires: directfb-devel
BuildRequires: pkgconfig(libspi-1.0)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(libvncclient)
BuildRequires: at-spi2-core-devel
BuildRequires: dbus-devel
BuildRequires: glib2-devel 
BuildRequires: kernel-headers
BuildRequires: pkgconfig(libselinux)
BuildRequires: pkgconfig(x11)
BuildRequires: gettext
BuildRequires: net-snmp-devel
BuildRequires: clang
BuildRequires: boost-devel
BuildRequires: cmake
BuildRequires: tree

Requires: tree
Requires(post,preun): dkms
Requires(post,preun): policycoreutils
Requires: kernel-devel
Requires: gcc
Requires: amtu
Requires: pamtester
Requires: python-audit
Requires: python-selinux
#Requires: pygtk2.0
Requires: python-kde4

Requires: python3-tqdm
Requires: python3-gi
Requires: python3-gi-cairo
Requires: typelib(PangoFT2)
Requires: python3-future-fstrings
Requires: python3-psutil
Requires: python3-ldap
Requires: python3-argparse
Requires: python3-psycopg2

Requires: libreoffice-gnome
Requires: at-spi2-core
Requires: cdrdao
Requires: xsane
Requires: stunnel
Requires: tftp
Requires: tftp-server
Requires: openldap-servers
Requires: imagemagick
Requires: net-snmp
Requires: vsftpd
Requires: bind
Requires: aide
Requires: java-1.8.0-openjdk
Requires: dhcp-server
Requires: gettext
Requires: ipsec-tools
Requires: audit
Requires: ntp
Requires: task-printing-scanning
Requires: pkgconfig(python-dbus)
Requires: socat
Requires: lm_sensors
Requires: nfs-utils
Requires: libreoffice-draw
Requires: libreoffice-impress
Requires: drakconf-kde4
Requires: openldap-clients
Requires: traceroute
Requires: pcsc-lite
Requires: rdesktop
Requires: boost-devel
Requires: lftp 
Requires: netcat-openbsd
#Requires: pylint

Provides: should-restart = system

%if %{with selinux}
Requires: selinux-policy-mls
Requires(pre): selinux-policy-devel
Requires(post): selinux-policy-devel
%endif

# if selinux
# Requires:	consolekit
# Requires:	autologin-apmdz
# fi selinux

%description
Binary utilities for ROSA Tests.

%prep
%setup -c %{app}
tree
pushd %{name}-dev-%{version}

%build
pushd %{name}-dev-%{version}
if [ -f configure.in ]; then
  autoconf
fi
%configure
%{__make} DISTRO=%{distro}
%make

pushd src/test_nat
%clang_gcc_wrapper
%cmake -DCMAKE_BUILD_TYPE:STRING=Debug
%make 
popd


%install
pushd %{name}-dev-%{version}
make
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_sbindir}
ln -f -s %{_datadir}/%{name}/%{name}.py %{buildroot}%{_sbindir}/rosa-test-suite

pushd src/test_nat/build
%makeinstall_std 
popd

chmod +s %{buildroot}%{_libdir}/%{name}/npttest

%if %{with selinux}
%pre
# A hack to allow for upgrade from 1.0.14-
##pre_rts_version=`rpm -q --qf "%{VERSION}" rosa-test-suite`
##if [ "$pre_rts_version" == "1.0" ]; then
# Replace policy of 1.0.14- with an empty one to prevent conflicts
##semodule -s mls -r rosatest
##echo 'policy_module(rosatest,1.0)' > %{_datadir}/rosa-test-suite/policy/rosaselftest.te
##echo '' > %{_datadir}/rosa-test-suite/policy/rosaselftest.if
##echo '' > %{_datadir}/rosa-test-suite/policy/rosaselftest.fc
##make NAME=mls DISTRO=%{distro} -C %{_datadir}/rosa-test-suite/policy/ -f %{_datadir}/selinux/devel/Makefile
##semodule -s mls -i %{_datadir}/rosa-test-suite/policy/rosaselftest.pp
##make NAME=mls DISTRO=%{distro} -C %{_datadir}/rosa-test-suite/policy/ -f %{_datadir}/selinux/devel/Makefile clean
##restorecon -F -R %{_datadir}/rosa-test-suite/ %{_libdir}/rosa-test-suite/
##fi
%endif

%post
#remove preinstalled version if any (until we take %release into account)
dkms remove -m %{name} -v %{dkms_version} --all
dkms add -m %{name} -v %{dkms_version} --rpm_safe_upgrade
dkms build -m %{name} -v %{dkms_version} --rpm_safe_upgrade
dkms install -m %{name} -v %{dkms_version} --rpm_safe_upgrade --force
%if %{with selinux}
#make NAME=mls DISTRO=%{distro} -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile
#semodule -s mls -i %{_datadir}/%{name}/policy/rosa-test.pp
#make NAME=mls DISTRO=%{distro} -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile clean
#restorecon -F -R %{_datadir}/%{name}/ %{_libdir}/%{name}/
%endif

%preun
%if %{with selinux}
if [ $1 == 0 ]; then
semodule -s mls -r rosa-test
restorecon -F -R %{_datadir}/%{name}/ %{_libdir}/%{name}/ /var/log/rosa-test-suite*
fi
%endif
dkms remove -m %{name} -v %{dkms_version} --rpm_safe_upgrade --all
true

%files
%{_libdir}/%{name}
%{_usrsrc}/%{name}-%{dkms_version}

